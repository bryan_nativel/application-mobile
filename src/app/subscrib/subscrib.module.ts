import { SubscribComponent } from "./subscrib.component";
import { SubscribRoutingModule } from "./subscrib-routing.module";
import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { IonicModule } from "@ionic/angular";
import { FormsModule } from "@angular/forms";
import { ReactiveFormsModule } from "@angular/forms";

@NgModule({
  declarations: [SubscribComponent],
  imports: [
    CommonModule,
    IonicModule,
    FormsModule,
    SubscribRoutingModule,
    ReactiveFormsModule,
  ],
})
export class SubscribModule {}
