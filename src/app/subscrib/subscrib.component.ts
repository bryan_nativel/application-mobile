import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { ApiUsersService } from "../api-user/api-user.service";
import { LoadingController } from "@ionic/angular";

@Component({
  selector: "app-subscrib",
  templateUrl: "./subscrib.component.html",
  styleUrls: ["./subscrib.component.scss"],
})
export class SubscribComponent implements OnInit {
  loginForm: FormGroup;
  emailError: boolean;
  loading;

  constructor(
    private fb: FormBuilder,
    public apiService: ApiUsersService,
    public loadingController: LoadingController
  ) {}
  async presentLoading() {
    const url = "http://localhost:4200";
    const loading = await this.loadingController.create({
      message: "Création du compte",
      duration: 5000,
    });
    await loading.present();

    const { role, data } = await loading.onDidDismiss();
    console.log("Loading dismissed!");
    this.loading = false;
    window.location.href = url;
  }

  ngOnInit() {
    this.loginForm = this.fb.group({
      fakeName: [],
      email: [
        "",
        Validators.compose([
          Validators.required,
          Validators.pattern("^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$"),
        ]),
      ],
      password: [],
    });
  }
  register() {
    if (!this.loginForm.get("email").errors) {
      const body = this.loginForm.value;

      this.apiService.createUser(body).subscribe((res) => {
        console.log("Created a customer");
      });
      this.emailError = false;
      this.loading = true;
      this.presentLoading();
    } else {
      this.emailError = true;
      this.loading = false;
    }
  }
}
