import { RouterModule, Routes } from "@angular/router";
import { SubscribComponent } from "./subscrib.component";
import { NgModule } from "@angular/core";

const routes: Routes = [
  {
    path: "",
    component: SubscribComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SubscribRoutingModule {}
