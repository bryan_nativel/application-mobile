import { Event } from "./../model/event";
import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";

@Injectable({
  providedIn: "root",
})
export class ApiEventService {
  public firstPage: string = "";
  public prevPage: string = "";
  public nextPage: string = "";
  public lastPage: string = "";
  apiURL: string = "http://localhost:3000/";

  constructor(private httpClient: HttpClient) {}

  public createEvent(event: Event) {
    return this.httpClient.post(`${this.apiURL}event/`, event);
  }

  public updateEvent(event: Event) {}

  public deleteEvent(id: number) {
    return this.httpClient.delete(`${this.apiURL}event/${id}`);
  }

  public getEventById(id: number) {
    return this.httpClient.get(`${this.apiURL}events/${id}`);
  }

  public getEvents(url?: string) {
    return this.httpClient.get<Event[]>(`${this.apiURL}event/`);
  }
}
