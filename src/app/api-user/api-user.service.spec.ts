import { TestBed } from "@angular/core/testing";

import { ApiEventService } from "./api-user.service";

describe("ApiEventService", () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it("should be created", () => {
    const service: ApiEventService = TestBed.get(ApiEventService);
    expect(service).toBeTruthy();
  });
});
