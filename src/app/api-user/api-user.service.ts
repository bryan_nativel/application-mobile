import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { User } from "../model/user";

@Injectable({
  providedIn: "root",
})
export class ApiUsersService {
  public headers: any;
  public firstPage: string = "";
  public prevPage: string = "";
  public nextPage: string = "";
  public lastPage: string = "";

  apiURL: string = "http://localhost:3000/";

  constructor(private httpClient: HttpClient) {
    this.headers = new HttpHeaders({
      "Content-type": "application/json",
    });
  }

  public createUser(user: User) {
    return this.httpClient.post<any>(`${this.apiURL}user/register`, user, {
      headers: this.headers,
    });
  }

  public updateUser(user: User) {}

  public deleteUser(id: number) {
    return this.httpClient.delete(`${this.apiURL}user/${id}`);
  }

  public getUserById(id: number) {
    return this.httpClient.get(`${this.apiURL}users/${id}`);
  }

  public getUsers(url?: string) {
    return this.httpClient.get<User[]>(`${this.apiURL}user/`);
  }
}
