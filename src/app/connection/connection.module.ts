import { ConnectionComponent } from "./connection.component";
import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { IonicModule } from "@ionic/angular";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { ConnectionRoutingModule } from "./connection-routing.module";

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ConnectionRoutingModule,
    ReactiveFormsModule,
  ],
  declarations: [ConnectionComponent],
})
export class ConnectionModule {}
