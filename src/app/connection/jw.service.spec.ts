import { TestBed } from '@angular/core/testing';

import { JwService } from './jw.service';

describe('JwService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: JwService = TestBed.get(JwService);
    expect(service).toBeTruthy();
  });
});
