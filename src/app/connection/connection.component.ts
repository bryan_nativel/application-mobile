import { JwService } from "./jw.service";
import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormGroup } from "@angular/forms";
import { LoadingController } from "@ionic/angular";

import { resolve } from "url";

@Component({
  selector: "app-connection",
  templateUrl: "./connection.component.html",
  styleUrls: ["./connection.component.scss"],
})
export class ConnectionComponent implements OnInit {
  constructor(
    protected fb: FormBuilder,
    protected service: JwService,
    public loadingController: LoadingController
  ) {}

  protected loading: true | false;
  protected data: FormGroup;

  async presentLoading() {
    const loading = await this.loadingController.create({
      message: "Connection",
      duration: 2000,
    });
    await loading.present();
    window.location.href = "/home";
  }

  ngOnInit() {
    this.data = this.fb.group({
      email: [],
      password: [],
    });
    //Si le user est deja connectée
    if (localStorage.getItem("access_token") !== null) {
      window.location.href = "/home";
    }
  }

  login() {
    if (!this.data.get("email").errors || !this.data.get("password").errors) {
      this.service
        .login(this.data.get("email").value, this.data.get("password").value)
        .subscribe((res) => {
          if (res.access_token) {
            this.presentLoading();
          }
        });
    }
  }
  creat() {
    window.location.href = "/inscription";
  }
}
