export class Event {
  id: number;
  picture: string;
  title: string;
  date: string;
  resume: string;
  typeEvents: string;
  lieu: string;
}
