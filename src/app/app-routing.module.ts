import { NgModule } from "@angular/core";
import { PreloadAllModules, RouterModule, Routes } from "@angular/router";

const routes: Routes = [
  {
    path: "home",
    loadChildren: () =>
      import("./home/home.module").then((m) => m.HomePageModule),
  },
  {
    path: "inscription",
    loadChildren: () =>
      import("./subscrib/subscrib.module").then((m) => m.SubscribModule),
  },
  {
    path: "connection",
    loadChildren: () =>
      import("./connection/connection.module").then((m) => m.ConnectionModule),
  },

  {
    path: "",
    redirectTo: "connection",
    pathMatch: "full",
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules }),
  ],
  exports: [RouterModule],
})
export class AppRoutingModule {}
