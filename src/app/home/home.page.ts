import { async } from "@angular/core/testing";
import { JwService } from "./../connection/jw.service";
import { ApiEventService } from "../api-event/api-event.service";
import { Component } from "@angular/core";

@Component({
  selector: "app-home",
  templateUrl: "home.page.html",
  styleUrls: ["home.page.scss"],
})
export class HomePage {
  events: {};
  constructor(
    private apiService: ApiEventService,
    private jwtService: JwService
  ) {}

  ngOnInit() {
    if (localStorage.getItem("access_token") == null) {
      window.location.href = "/connection";
    }
    this.apiService.getEvents().subscribe((res) => {
      this.apiService.getEvents(this.apiService.nextPage).subscribe((res) => {
        this.events = res;
      });
    });
  }
  logout() {
    this.jwtService.logout();
    window.location.href = "/connection";
  }
}
